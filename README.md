# Demo Setup for Kernel Module Development

1. Configure  `.vscode/c_cpp_properties.json` and configure the path for `toolchain-dir` and `linux-kernel-sources` with an absolute path.
2. Change `KERNEL_SRC_TARGET` and `ARM_TOOLCHAIN` in Makefile
3. To manually compile: run `make host` or `make target`.
4. To run from VS Code, hit `Shift + CTRL + B` and select host or target.