obj-m = mod_hello.o mod_hrtimer.o
KERNEL_SRC_HOST = /lib/modules/$(shell uname -r)/build


# TODO anpassen
KERNEL_SRC_TARGET = ~/esl/linux-stable

# TODO anpassen
ARM_TOOLCHAIN = ~/esl/toolchain/bin/arm-linux-gnueabihf-

target:
	@echo "Building for target"
	@make \
		CROSS_COMPILE=$(ARM_TOOLCHAIN) \
		ARCH=arm \
		-C $(KERNEL_SRC_TARGET) \
		M=$(PWD) \
		modules

	@rm  -f  *.o  *.mod.c .*.cmd modules.order Module.symvers .cache.mk *.mod

host:
	@echo "Building for host"
	@make  -C $(KERNEL_SRC_HOST)  M=$(PWD)  modules
	@rm  -f  *.o  *.mod.c .*.cmd modules.order Module.symvers .cache.mk

clean:
	make -C $(KERNEL_SRC_HOST) M=$(PWD) clean
	make -C $(KERNEL_SRC_TARGET) M=$(PWD) clean

install:
	scp mod_hello.ko root@esl.local:/root
	scp mod_hrtimer.ko root@esl.local:/root

# indents in rules above have to be TAB characters! Check&correct after copy&paste out of PDF!