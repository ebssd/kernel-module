#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt


def main():
    print("stats")
    times = np.genfromtxt("./statistics/data.txt")
    deltas = []
    relative_values = []
    idx = 0

    for x in np.nditer(times):
        if(idx == 0):
            idx += 1
            continue

        deltas.append((x - times[idx-1])/1e3 - 1000)
        relative_values.append((x - times[0])/1e3 - 1000*idx)

        idx += 1

    fig, plots = plt.subplots(2)
    plots[0].set_title(
        "Histogram: delta in [us] for {} samples (expected 0[ms])".format(len(deltas)))
    plots[0].hist(deltas, bins='auto')

    plots[1].set_title(
        "Histogram: delta to expected time relative to startime in [us] for {} samples (expected 0[ms])".format(len(relative_values)))
    plots[1].hist(relative_values, bins=100, range=(-5, -2))
    # plots[1].set_yscale('log')
    plt.show()


if __name__ == "__main__":
    main()
